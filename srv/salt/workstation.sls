include:
    - common

workstation:
    pkg.installed:
        - pkgs:
            - vlc
            - chrome
            - libreoffice

windirstat: chocolatey.installed
vscode: chocolatey.installed
conemu: chocolatey.installed
teamviewer: chocolatey.installed
sublimetext3: chocolatey.installed
{# sublimetext3.packagecontrol: chocolatey.installed #}
github: chocolatey.installed