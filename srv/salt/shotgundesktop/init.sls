shotgundesktop:
  1.5.4:
    full_name: Shotgun Desktop 1.5.4
    installer: 'salt://shotgundesktop/ShotgunInstaller_Current.exe'
    install_flags: '/S'
    uninstaller: 'salt://shotgundesktop/ShotgunInstaller_Current.exe'
    uninstall_flags: ''
    msiexec: False
    reboot: False
