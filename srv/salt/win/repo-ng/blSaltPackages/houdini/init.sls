houdini:
  '17.5.293':
    full_name: 'Houdini 17.5.293'
    installer: 'salt://win/repo-ng/blSaltPackages/houdini/bin/17.5/houdini-17.5.293-win64-vc141.exe'
    install_flags: '/S /AcceptEULA=Yes /FileAssociations=Yes /Registry=Yes /StartMenu=Yes /HoudiniServer=Yes' 
    uninstaller: 'C:\Program Files\Side Effects Software\Houdini 17.5.293\Uninstall Houdini.exe'
    uninstall_flags: '/S'
    msiexec: False
    locale: en_US
    reboot: False
  '17.0.506':
    full_name: 'Houdini 17.0.506'
    installer: 'salt://win/repo-ng/blSaltPackages/houdini/bin/17.0.506/houdini-17.0.506-win64-vc141.exe'
    install_flags: '/S /AcceptEULA=Yes /FileAssociations=Yes /Registry=Yes /StartMenu=Yes /HoudiniServer=Yes' 
    uninstaller: 'C:\Program Files\Side Effects Software\Houdini 17.0.506\Uninstall Houdini.exe'
    uninstall_flags: '/S'
    msiexec: False
    locale: en_US
    reboot: False

{#
    '/S /MainApp=Yes /ApprenticeLicensing=No /DesktopIcon=No /FileAssociations=No /HoudiniServer=No /HQueueServer=No /HQueueClient=No /IndustryFileAssociations=No /LicenseServer=No /Registry=No /StartMenu=No'
    start /WAIT houdini-X.Y.ZZZ-win64-vc141.exe /S /MainApp=Yes /InstallDir=H:\houdini_distros\hfs.windows-x86_64 /ApprenticeLicensing=No /DesktopIcon=No /FileAssociations=No /HoudiniServer=No /HQueueServer=No /HQueueClient=No /IndustryFileAssociations=No /LicenseServer=No /Registry=No /StartMenu=No

    start /WAIT houdini-17.5.293-win64-vc141.exe /S /MainApp=Yes /InstallDir=H:\houdini_distros\hfs.windows-x86_64 /ApprenticeLicensing=No /DesktopIcon=No /FileAssociations=No /HoudiniServer=No /HQueueServer=No /HQueueClient=No /IndustryFileAssociations=No /LicenseServer=No /Registry=No /StartMenu=No

    /S /MainApp=Yes /ApprenticeLicensing=No /DesktopIcon=No /FileAssociations=No /HoudiniServer=No /HQueueServer=No /HQueueClient=No /IndustryFileAssociations=No /LicenseServer=No /Registry=No /StartMenu=No

     #}