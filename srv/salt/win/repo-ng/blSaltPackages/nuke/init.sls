nuke:
  '11.0':
    full_name: 'Nuke 11.0v4'
    installer: 'salt://win/repo-ng/blSaltPackages/nuke/bin/11/Nuke11.0v4-win-x86-release-64/Nuke11.0v4-win-x86-release-64.exe'
    install_flags: '/silent' 
    uninstaller: 'C:\Program Files\Nuke11.0v4\unins000.exe'
    uninstall_flags: '/silent'
    msiexec: False
    locale: en_US
    reboot: False
  '11.1':
    full_name: 'Nuke 11.1v6'
    installer: 'salt://win/repo-ng/blSaltPackages/nuke/bin/11/Nuke11.1v6-win-x86-release-64/Nuke11.1v6-win-x86-release-64.exe'
    install_flags: '/silent' 
    uninstaller: 'C:\Program Files\Nuke11.1v6\unins000.exe'
    uninstall_flags: '/silent'
    msiexec: False
    locale: en_US
    reboot: False
  '11.2':
    full_name: 'Nuke 11.2v6'
    installer: 'salt://win/repo-ng/blSaltPackages/nuke/bin/11/Nuke11.2v6-win-x86-release-64/Nuke11.2v6-win-x86-release-64.exe'
    install_flags: '/silent' 
    uninstaller: 'C:\Program Files\Nuke11.2v6\unins000.exe'
    uninstall_flags: '/silent'
    msiexec: False
    locale: en_US
    reboot: False
  '11.3':
    full_name: 'Nuke 11.3v4'
    installer: 'salt://win/repo-ng/blSaltPackages/nuke/bin/11/Nuke11.3v4-win-x86-release-64/Nuke11.3v4-win-x86-release-64.exe'
    install_flags: '/silent' 
    uninstaller: 'C:\Program Files\Nuke11.3v4\unins000.exe'
    uninstall_flags: '/silent'
    msiexec: False
    locale: en_US
    reboot: False

