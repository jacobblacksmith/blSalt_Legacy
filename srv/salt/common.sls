install python 2 and 3:
  pkg.installed:
    - pkgs:
      - python3_x64
      - python2_x64

chocolatey:
  pkg.installed

quicktime: chocolatey.installed
7zip.install: chocolatey.installed

add user account for blacksmith:
  user.present:
    - name: Blacksmith
    - home: /home/blacksmith
