include:
  - workstation

install houdini:
  pkg.installed:
    - pkgs:
      - houdini: '17.5.293'

install nuke:
  pkg.installed:
    - pkgs:
      - nuke: '11.3'