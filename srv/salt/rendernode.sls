{# include:
    - common #}

install_nuke11.3:
  pkg.installed:
      - pkgs:
          - nuke: '11.3'

install_nuke11.2:
  pkg.installed:
      - pkgs:
          - nuke: '11.2'

install_houdini17.5:
  pkg.installed:
      - pkgs:
          - houdini: '17.5.293'

install_houdini17.0:
  pkg.installed:
      - pkgs:
          - houdini: '17.0.506'

license_houdini:
  cmd.run:
    - name: hserver -S lic01 >> c:\hserver_output.txt
    - creates: c:\hserver_output.txt