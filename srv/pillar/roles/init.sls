{% set roles = salt['match.search_by']({
    'workstation': [],
    'master': ['salt-master'],
    'rendernode': ['rnd_*', 'wrk_*'],
    'fx': ['tinker']
}, minion_id=grains.id) %}

# Make the filtered data available to Pillar:
roles: {{ roles | yaml() }}